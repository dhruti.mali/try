# mongo.py

from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'mydb'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/mydb'

mongo = PyMongo(app)

@app.route('/user', methods=['POST'])
def add_star():
  user = mongo.db.user
  name = request.json['name']
  official_email = request.json['official_email']
  user_id = request.json['user_id']
  insurance_type = request.json['insurance_type']
  member_type = request.json['member_type']
  id_id = user.insert({'name': name, 'official_email': official_email, 'user_id': user_id , 'insurance_type': insurance_type  , 'member_type': member_type })
  new_user = user.find_one({'_id': id_id })
  output = {'name' : new_user['name'], 'official_email' : new_user['official_email'], 'user_id' : new_user['user_id'], 'insurance_type' : new_user['insurance_type'], 'member_type' : new_user['member_type']}
  return jsonify({'result' : output})

if __name__ == '__main__':
    app.run(debug=True)
